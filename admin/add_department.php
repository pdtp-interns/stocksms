<?php 	

include('header.php');
include('database_connection.php'); 

if(isset($_POST['submit'])) {

    $department_name=$_POST['department_name'];
    $query = "INSERT INTO department(department_name) VALUES ('$department_name')";
    $result = mysqli_query($connect, $query);
}

?>
 
<div class="page-container">  
<div class="left-content">
     <div class="mother-grid-inner">
      <?php
      include('navbar.php');

      ?>
        
      <div class="container" style="margin-top: 20px;">
      <div class="row justify-content-center">
          <form action="" method="POST">
          <input type="hidden" name="department_id" value="">
          <div class="form-group">
            <label>Department Name</label>  
              <select name="department_name" class="form-control" style="width: 260px;"> 
                <option name="department" value="" selected="">Select Department</option>
                    <option>IT</option>
                    <option>Finance</option>
                    <option>Library</option>
                    <option>Examination</option>
              </select>  
          </div>
          <div class="form-group">
          <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Add</button>
          </div>
          </div>
          </form>
      </div>
      </div>
    </div>
</div>
</div>

<?php 
include('sidewrapper.php');
include('footer.php');
?>