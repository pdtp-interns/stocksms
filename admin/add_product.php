<?php 
include('header.php');
?>

<body>	
<div class="page-container">	
<div class="left-content">
<div class="mother-grid-inner">
	   	
  <?php
include('navbar.php');
?>
        <?php 
 ?>

    <div class="panel panel-default" style="margin-left: 115px;">          
 	    <div class="container-fluid" style="margin-top: 40px;">
        <div class="row justify-content-center">
	        <form action="manage_product.php" method="POST">
             <input type="hidden" name="product_id" value=" ">
		         <div class="form-group">
		         <label>Product Name</label>
             <select name="product_name" class="form-control" style="width: 260px;"> 
                  <option name="product" value="" selected="">Browse for an Item</option>
                  <option>Monitors</option>
                  <option>Pens</option>
                  <option>Books</option>
                  <option>Stamps</option>
             </select>  
		         </div>
		      <div class="form-group">
		      <label>Product Quantity</label>
		      <input type="text" name="product_quantity" class="form-control" value="" placeholder="Product Quantity" style="width: 260px;">
		      </div>

		      <div class="form-group">
		      <button type="submit" class="btn btn-primary" name="submit">Add</button>
		      </div>

	        </form>
        </div>
      </div>
    </div>
</div>
</div>
</div>

<!--slider menu-->
<?php
include('sidewrapper.php');
include('footer.php');
?>