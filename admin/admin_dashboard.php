<!DOCTYPE html>
<html>
<head lang="en">
   <link rel="stylesheet" href="styles/css/bootstrap.min.css">
  <!-- <link rel="stylesheet" href="css/bootstrap.min.css">
 --><script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>

  <script src="styles/css/bootstrap.min"></script>
  <script src="styles/js/bootstrap.min"></script>
  <script type="text/javascript" src="styles/js/bootstrap.js"></script>
  <script type="text/javascript" src="styles/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="styles/js/jquery.js"></script>
  
   <link rel="stylesheet" type="text/css" href="style.css">
  <meta charset="utf-8" name="viewport" content="width=device-width,initial-scale=1">
  <title>admin</title>
</head>
<body>

   <div id="page-content-wrapper" class="bg-light" style="margin-top:0px;margin-left: 0px; ">
  <nav class="navbar navbar-expand-sm bg-info navbar-light" style="height: 60px;">
    <div class="container">
      <div class="navbar-header">
        <a class="navbar-brand" href="#">Logo</a>
      </div>
    <ul class="nav navbar-nav navbar-right">
    
      <li class="dropdown">
        <a href="#" style="color: red; font-size: 110%" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><?php echo $_SESSION['user_email']; ?><b class="caret"></b></a>
         <ul class="dropdown-menu">

        <li>
          <a href="logout.php"><i class="fa fa-fw fa-power-off"></i>log out</a>
        </li>
        <li class="divider"></li>
        <li>
          <a href="#"><i class="fa fa-fw fa-power-off"></i>Edit Profile</a>
        </li>

       </ul> 
      </li>

    </ul>
 
    </div>
  </nav>
    <div class="modal" id="mymodal">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-body">
          <div class="modal-header">
            <button class="close" type="button" data-dismiss="modal">x</button>
          </div>
            
          <h2  style="font-family: 'Bree Serif', serif; font-size:3em;"> Select range</h2>
              <hr>
            <form method="post" action="<?php $_SERVER['PHP_SELF']; ?>" id="form2">
              <label style="font-family: 'Fira Sans', sans-serif; font-size:1.6em;">From</label>
                <select name="range" style="width:75%;height:25px;">
                  <option value="all">All</option>
                  <option value="FE">FE</option>
                  <option value="FH">FH</option>
                  <option value="FI">FI</option>
                  <option value="FJ">FJ</option>
                  <option value="FM">FM</option>
                  <option value="FN">FN</option>
                  <option value="FG">FG</option>
                  <option value="FY">FY</option>
                  <option value="FZ">FZ</option>
                </select>
              <hr/>
              <input name="getFuso" class="btn btn-info" type="submit" value="Fetch" />
            </form>
    </div>
    </div>
    </div>
    </div>
  <div class="container">
  <div class="row">
    <div class="col-md-4">
    	<div class="card mx-auto" style="width: 20rem;">
    		<img class="card-img-top mx-auto" src="log.png" alt="Card image cap">
    		<div class="card-body">
    			<h4 class="card-title"> Profile info</h4>
    			<p class="card-text"><i class="fa fa-user">&nbsp;</i>Purity Kamene</p>
    			<p class="card-text"><i class="fa fa-user">&nbsp;</i>Admin</p>
    			<p class="card-text">Last login:xxxx-xx-xx</p>
    			<a href="#" class="btn btn-primary"><i class="fa fa-edit">&nbsp;</i>Edit Profile</a>
    		</div>
    	</div>
    </div>

    <div class="col-md-8">
    <div class="jumbotron" style="width: 100%;height: 100%;">
        <h1>Welcome Admin,</h1>
      <div class="row">
        <div class="co l-sm-6">
       <!--    <img class="card-img-top mx-auto" src="log.png" alt="Card image cap" style="width: 160px; height: 160px;">
 -->
        </div>
     
      <div class="col-sm-6">
      <div class="card">
            <div class="card-body">
              <h4 class="card-title">New orders</h4>
              <p class="card-text">with supporting text below as a natural lead-in to additional content.</p>
              <a href="#" class="btn btn-primary">New orders</a>
            </div>
      </div> 
      </div>

      </div>

    </div>
    </div>
  </div>
  </div>

<div class="container">
  <div class="row">
    <div class="col-md-4">
      <div class="card">
        <div class="card-body" style="width: 20rem;"">
          <h4 class="card-title">Products</h4>
          <p class="card-text"> Here you can manage  your products and add new products</p>
          <a href="add_product.php" class="btn btn-primary">Add</a>
          <a href="manage_product.php" class="btn btn-primary">Manage</a>
        </div>
      </div>
    </div>
        <div class="col-md-4">
      <div class="card">
        <div class="card-body" style="width: 20rem;"">
          <h4 class="card-title">Users</h4>
          <p class="card-text"> Here you can manage  your Users and add new users</p>
          <a href="#" class="btn btn-primary" role="button" data-toggle="modal" data-target="#add_user" >Add</a>   
          <a href="manage_users.php" class="btn btn-primary">Manage</a>
        </div>
      </div>
    </div>

        <div class="col-md-4">
      <div class="card">
        <div class="card-body" style="width: 20rem;"">
          <h4 class="card-title">Department</h4>
          <p class="card-text"> Here you can manage  your department and add new department</p>
          <a href="add_department.php" class="btn btn-primary">Add</a>      
            <a href="#" class="btn btn-primary" role="button" data-toggle="modal" data-target="#mymodal" >Manage</a>
        </div>
      </div>
    </div>
  </div>

</div>

 <div class="modal" id="add_user" style="margin-top: 30px;">
    <div class="modal-dialog" style="width: 500px;">
      <div class="modal-content">
        <div class="modal-body">
          <div class="modal-header">
            <button class="close" type="button" data-dismiss="modal">x</button>
          </div>

  <div class="header" style="width: 450px;">
  <h2>Register</h2>
</div>

<form method="post" action="register_process.php" style=" width: 450px;">
  <div class="input-group">
    <label><strong> First Name </strong></label>
    <input type="text" name="first_name" value="">
    </div>
    <div class="input-group">
    <label>Last Name</label>
    <input type="text" name="last_name" value="">
    </div>
      <div class="input-group">
    <label>Department</label>
       <select name="department_name" class=" "> 
          <option name="department_name1" value="" selected=" ">Finance</option>
            <?php 
                                      $connect = mysqli_connect('localhost','root','','stock_system');
                                      $query = "select * FROM department";
                                      $result = mysqli_query($connect,$query);
                                      while ($output = mysqli_fetch_assoc($result)) {
                                        $department_id = $output["department_id"]; 
                                        $department_name = $output["department_name"];                                     ?>
                                     <option name="department_name1" value="<?php  echo $output["department_name"]; ?>"><?php echo $output["department_name"]; ?></option>
                                     <?php } ?>
        </select>
    </div>
     <div class="input-group">
    <label>User Role</label>
    <select name="user_role" class="form-control"> 
         <option name="user_role" value="Staff" selected="">Staff</option>
         <option name="user_role" value="Approver" selected=" ">Approver</option>
         <option name="user_role" value="Admin">Admin</option>
     </select>

    </div>
  <div class="input-group">
    <label>Email</label>
    <input type="text" name="user_email" value="">
    </div>
  <div class="input-group">
    <label>Password</label>
    <input type="password" name="user_password">
    </div>
  <div class="input-group">
    <button type="submit" name="submit" class="btn">Register</button>
  </div>
</form>
 </div>
      </div>
         </div>
        </div>
      
</div>
</body>
</html>