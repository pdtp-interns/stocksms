
<?php 
include('header.php');
?>

<body>	
<div class="page-container">	
   <div class="left-content">
	   <div class="mother-grid-inner">
<?php
include('navbar.php');
?>

<div class="inner-block">
<!--market updates updates-->
   <div class="market-updates">
      <div class="col-md-4 market-update-gd">
        <div class="market-update-block clr-block-2">
         <div class="col-md-8 market-update-left">
                      <?php
            $select = "SELECT * FROM user_details WHERE user_role='Staff' ";
            $query = mysqli_query($connect,$select);
            $staff_counts = mysqli_num_rows($query);
            echo "<div class = 'huge'>{$staff_counts}</div>"
            ?>
            <h4>Registered Requestors</h4>
           </div>
          <div class="col-md-4 market-update-right">
            <i class="fa fa-user fa-5x"> </i>
          </div>
          <div class="clearfix"> </div>
        </div>
      </div>
      <div class="col-md-4 market-update-gd">
        <div class="market-update-block clr-block-2">
         <div class="col-md-8 market-update-left">
          <?php
            $select = "SELECT * FROM user_details WHERE user_role='Approver'";
            $query = mysqli_query($connect,$select);
            $approver_counts = mysqli_num_rows($query);
            echo "<div class = 'huge'>{$approver_counts}</div>"
            ?>
          <h4>Registered Approvers</h4>
           </div>
          <div class="col-md-4 market-update-right">
            <i class="fa fa-user fa-5x"> </i>
          </div>
          <div class="clearfix"> </div>
        </div>
      </div>
      <div class="col-md-4 market-update-gd">
        <div class="market-update-block clr-block-2">
         <div class="col-md-8 market-update-left">
          <?php
            $select = "SELECT * FROM user_details WHERE user_role='Admin'";
            $query = mysqli_query($connect,$select);
            $admin_counts = mysqli_num_rows($query);
            echo "<div class = 'huge'>{$admin_counts}</div>"
            ?>
          <h4>Registered Admins</h4>
           </div>
          <div class="col-md-4 market-update-right">
            <i class="fa fa-user fa-5x"> </i>
          </div>
          <div class="clearfix"> </div>
        </div>
      </div>
       <div class="clearfix"> </div>
    </div>
    <br>
    <hr>
     <div class="market-updates">
      <div class="col-md-4 market-update-gd">
        <div class="market-update-block clr-block-1">
          <div class="col-md-8 market-update-left">
            <?php
            $select = "SELECT * FROM request_details WHERE status = 'Approved'";
            $query = mysqli_query($connect,$select);
            $Approved_request_counts = mysqli_num_rows($query);
            echo "<div class = 'huge'>{$Approved_request_counts}</div>"
            ?>
            <h4>Approved Requests</h4>
           </div>
          <div class="col-md-4 market-update-right">
            <i class="fa fa-file-text-o"> </i>
          </div>
          <div class="clearfix"> </div>
        </div>
      </div>
      <div class="col-md-4 market-update-gd">
        <div class="market-update-block clr-block-1">
          <div class="col-md-8 market-update-left">
            <?php
            $select = "SELECT * FROM issued_items WHERE proportion_issued = 'Fully'";
            $query = mysqli_query($connect,$select);
            $Approved_request_counts = mysqli_num_rows($query);
            echo "<div class = 'huge'>{$Approved_request_counts}</div>"
            ?>
            <h4>Fully Fulfilled Requests</h4>
           </div>
          <div class="col-md-4 market-update-right">
            <i class="fa fa-file-text-o"> </i>
          </div>
          <div class="clearfix"> </div>
        </div>
      </div>
<div class="col-md-4 market-update-gd">
        <div class="market-update-block clr-block-1">
          <div class="col-md-8 market-update-left">
            <?php
            $select = "SELECT * FROM issued_items WHERE proportion_issued = 'Fully'";
            $query = mysqli_query($connect,$select);
            $Approved_request_counts = mysqli_num_rows($query);
            echo "<div class = 'huge'>{$Approved_request_counts}</div>"
            ?>
            <h4>Partially Fulfilled</h4>
           </div>
          <div class="col-md-4 market-update-right">
            <i class="fa fa-file-text-o"> </i>
          </div>
          <div class="clearfix"> </div>
        </div>
      </div>
</div>
</div>
</div>
<!--slider menu-->
        <?php
include('sidewrapper.php');
include('footer.php');
?>