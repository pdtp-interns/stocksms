<?php 

include('database_connection.php');
session_start();
 ERROR_REPORTING(E_ERROR || E_PARSE);

if(!isset($_SESSION["user_id"])){
    header('Location:../index.php');
}


include('header.php');
?>

<body>	
<div class="page-container">	
   <div class="left-content">
	   <div class="mother-grid-inner">
<?php
include('navbar.php');
?>

<div class="panel panel-default" style="margin-top: 15px;">
              <!-- Default panel contents -->
              <div class="panel-heading bg-info">Manage Requests</div>
<div class="container-fluid">
<?php
$connect = mysqli_connect('localhost', 'root', '', 'stock_system');
$sql = "SELECT request_details.status,request_details.department_name,request_details.product_name,user_details.user_id, user_details.first_name,user_details.user_email,user_details.last_name,request_details.request_id,request_details.request_date,request_details.product_quantity FROM request_details
JOIN user_details ON 
 request_details.user_id = user_details.user_id
WHERE status ='Approved'
";
$result = mysqli_query($connect,$sql);
?>
<div class="row justify-content-center">
<div class="table-responsive">
             <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
 
                       <tr>
                        <th>#</th>
                            <th>Request ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Department Name</th>
                            <th>Product Name</th>
                            <th>Product Quantity</th>
                            <th>Request Date</th>
                            <th>Status</th>
                         
                        </tr>
                    </thead>
                      <tbody>
                          <?php
while ($row =mysqli_fetch_assoc($result)){ ?>
<form action="dashboard.php" method="POST">
  <tr>
     <td><input type="checkbox" name="request_id" value="<?php echo $row['request_id']?>"</td>
    <td><?php echo $row['request_id']; ?></td>
    <td><?php echo $row['first_name']; ?></td>
    <td><?php echo $row['last_name']; ?></td>
    <td><?php echo $row['department_name']; ?></td>
    <td><?php echo $row['product_name']; ?></td>
    <td><?php echo $row['product_quantity']; ?></td>
    <td><?php echo $row['request_date']; ?></td>
    <td><button id="product_status" name="product_status" style=" " class="btn btn-success">Active</button>
</td>
  </tr>
  <?php ERROR_REPORTING(E_ERROR || E_PARSE); ?>
<?php



 }

 ?>
                      </tbody>
             </table>
           </div>
    <input type="submit" class="btn btn-info" value="ISSUE" name="btn_issue" id="btn_issue">
    
            <!--<input type="submit" class="btn btn-info" value="Export" name="export_excel">
    <a href="#" class="btn btn-info" role="button" name="export_excel">Export to Excel</a> -->
    </form>
         <?php //include_once('excel.php'); ?>
</div>
</div>
</div>
</div>
<?php
 if(isset($_POST["btn_issue"]) && ($_POST["btn_issue"]) != "0"){
	 
	 
	 
	  	  $request_id = $_POST["request_id"];	 

      $select = "SELECT * FROM department WHERE   department_id = '".$_SESSION['department_id']."' ";
  $result = mysqli_query($connect,$select);
  $row =mysqli_fetch_assoc($result);
  $department_id = $row["department_id"];
  $_SESSION["department_id"] = $department_id;

   
  $select = "SELECT * FROM request_details WHERE request_id = '$request_id'";
  $result = mysqli_query($connect,$select);
  $row =mysqli_fetch_assoc($result);
  $request_id = $row["request_id"];
  $user_id = $row["user_id"];
  $product_name = $row["product_name"];
  $product_quantity = $row["product_quantity"];
  $issued_date = date('Y-m-d');

   $sql = "INSERT INTO issued_items(request_id,user_id,department_id,product_name,product_quantity,issued_date,proportion_issued) VALUES('$request_id','$user_id','".$_SESSION["department_id"]."','$product_name','$product_quantity','$issued_date','Fully')
   ";
   $result = mysqli_query($connect,$sql);
 }


                        
?>


</div>
</div>
<!--slider menu-->
        <?php
include('sidewrapper.php');
include('footer.php');
?>