CREATE TABLE IF NOT EXISTS user_details
(
 first_name varchar(255) NOT NULL,
 last_name varchar(255) NOT NULL,
 department_id varchar(255) NOT NULL,
 user_role varchar(255) NOT NULL,
 user_id int(11) NOT NULL AUTO_INCREMENT,
 user_email varchar(255) NOT NULL,
 user_password int(50) NOT NULL,
 PRIMARY KEY (user_id)
  )ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;



CREATE TABLE IF NOT EXISTS product_details
(
	product_id int(11) NOT NULL AUTO_INCREMENT,
	product_name varchar(255) NOT NULL,
    product_quantity int(11) NOT NULL,
    product_status varchar(255) NOT NULL,
   	PRIMARY KEY (product_id)	
)ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS department
(
	department_id int(11) NOT NULL AUTO_INCREMENT,
	department_name varchar(255) NOT NULL,
   	PRIMARY KEY (department_id)	
)ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS suppliers
(
	supplier_id int(11) NOT NULL AUTO_INCREMENT,
	supplier_name varchar(255) NOT NULL,
   	PRIMARY KEY (supplier_id)	
)ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS supplier_products
(
	product_supplied_id int(11) NOT NULL AUTO_INCREMENT,
	product_supplied_name varchar(255) NOT NULL,
  product_price varchar(255) NOT NULL,
	supplier_id int(11) NOT NULL,
   	PRIMARY KEY (product_supplied_id),
    FOREIGN KEY (supplier_id) REFERENCES suppliers(supplier_id)
)ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS purchase_order
(
 po_id int(11) NOT NULL AUTO_INCREMENT,
 user_id int(11) NOT NULL,
 order_date date NOT NULL,
 supplier_name varchar(255) NOT NULL,
 <!--product_id int(11) NOT NULL,-->
 product_name varchar(255) NOT NULL,
 product_quantity int(11) NOT NULL,
 unit_Price varchar(255) NOT NULL,
 total varchar(255) NOT NULL,
 PRIMARY KEY (po_id)
  )ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS request_details
(
 user_id int(11) NOT NULL,
 request_id int(18) NOT NULL AUTO_INCREMENT,
 department_id int(11) NOT NULL,
 department_name varchar(255) NOT NULL,
 product_name varchar(255) NOT NULL,
 product_quantity int(11) NOT NULL,
 request_date date NOT NULL,
 request_priority varchar(200) NOT NULL,
 status varchar(255) NOT NULL,
 PRIMARY KEY (request_id),
 FOREIGN KEY (user_id) REFERENCES user_details(user_id), 
 FOREIGN KEY (department_id) REFERENCES department(department_id)
)ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS issued_items
(
 issue_id int(11) NOT NULL AUTO_INCREMENT,
 request_id int(18) NOT NULL,
 user_id int(11) NOT NULL,
 department_id int(11) NOT NULL,
 product_name varchar(255) NOT NULL,
 product_quantity int(11) NOT NULL,
 issued_date date NOT NULL,
 proportion_issued varchar(200) NOT NULL,
 PRIMARY KEY (issue_id),
 FOREIGN KEY (user_id) REFERENCES user_details(user_id),
 FOREIGN KEY (department_id) REFERENCES department(department_id)
)ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

INSERT INTO suppliers (supplier_name) VALUES
 ('Chandaria'),
 ('Kcc'),
 ('Exe'),
 ('Longhorn');
 
 INSERT INTO department (department_name) VALUES
 ('Finance'),
 ('IT'),
 ('Library'),
 ('HR'),
 ('Examination');

 INSERT INTO user_details(first_name,last_name,department_id,user_role,user_id,user_email,user_password) VALUES
 ('admin','admin','','Admin','100','puritykamene21@gmail.com','5555');