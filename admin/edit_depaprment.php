	<?php 
  session_start();
include('header.php');
include('database_connection.php'); 
?>
<?php 

if (isset($_GET['edit'])) {
  $department_id = $_GET['edit'];
  $update = true;
    $query = "SELECT * FROM department WHERE department_id= $department_id";
  $result = mysqli_query($connect,$query);
  if (count($result)==1) {
    $row = $result->fetch_array();
    $department_name= $row ['department_name'];
  }
}
?>

<div class="page-container">  
   <div class="left-content">
     <div class="mother-grid-inner">
      <?php
      include('navbar.php');

      ?>
  <div class="container" style="margin-top: 20px;">
  <div class="row justify-content-center">

 <form action="update_department.php?department_id=<?php echo $department_id ?>"  method="POST">
        <input type="hidden" name="department_id" value="<?php echo $department_id; ?>">
    <div class="form-group">
    <label>Department Name</label>  
    <input type="text" name="department_name" class="form-control" value="<?php echo $department_name; ?>" placeholder="Type Department Name ">
    </div>
  <div class="form-group">
		<button type="submit" class="btn btn-info" name="update">Update</button>
	</div>

	</form>
   </div>
   </div>
</div>
</div>
</div>

<?php 
include('sidewrapper.php');
include('footer.php');
?>
