	<?php 
include('header.php');
include('database_connection.php'); 
?>
<?php 

if (isset($_GET['edit'])) {
  $product_id = $_GET['edit'];
  $update = true;
    $query = "SELECT * FROM product_details WHERE product_id= $product_id";
  $result = mysqli_query($connect,$query);
  if (count($result)==1) {
    $row = $result->fetch_array();
    $product_name= $row ['product_name'];
    $product_quantity = $row['product_quantity'];
  }
}
?>

<div class="page-container">  
   <div class="left-content">
     <div class="mother-grid-inner">
      <?php
      include('navbar.php');

      ?>
  <div class="container" style="margin-top: 20px;">
  <div class="row justify-content-center">

 <form action="update_product.php?product_id=<?php echo $product_id ?>"  method="POST">
        <input type="hidden" name="product_id" value="<?php echo $product_id; ?>">
    <div class="form-group">
    <label>Product Name</label>  
    <input type="text" name="product_name" class="form-control" value="<?php echo $product_name; ?>" placeholder="Type Product Name ">
    </div>
    <div class="form-group">
    <label>Product Quantity</label>
    <input type="text" name="product_quantity" class="form-control" value="<?php echo $product_quantity; ?>" placeholder="Product Quantity" >
    </div>
  <div class="form-group">
		<button type="submit" class="btn btn-info" name="update">Update</button>
	</div>

	</form>
   </div>
   </div>
</div>
</div>
</div>

<?php 
include('sidewrapper.php');
include('footer.php');
?>
