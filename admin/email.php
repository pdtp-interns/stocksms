<?php 
$error ='';
$name = '';
$email = '';
$subject = '';
$message = '';

function clean_text($string)
{
	$string = trim($string);
	$string = stripslashes($string);
	$string = htmlspecialchars($string);
	return $string;
}
if (isset($_POST["submit"])) {
	if (empty($_POST["name"])) 
	{
	$error.='<p> <label class = "text-danger">Please  enter your name</label></p>';
	}
	else
	{
$name = clean_text($_POST["name"]);
if (!preg_match("/^[a-zA-Z ]*$/", $name)) 
       { 
	$error.='<p> <label class = "text-danger">only letters and white space are allowed</label></p>';
       }
	}

	if (empty($_POST["email"])) 
	{
		$error.='<p> <label class = "text-danger">please enter your email</label></p>';
	}
	else
	{
		$email = clean_text($_POST["email"]);
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
		{
			$error.='<p> <label class = "text-danger">Invalid email format</label></p>';
		}
	}

	if (empty($_POST["subject"])) 
	{
		$error.='<p> <label class = "text-danger">Subject is required</label></p>';	
	}
	else
	{
		$subject = clean_text($_POST["subject"]);
	}

	if (empty($_POST["message"]))
	 {
		$error.='<p> <label class = "text-danger">Message is required</label></p>';	

	}
	else
	{
   	$message = clean_text($_POST["message"]);
	}
	if ($error != '') 
	{
		use \PHPMailer\PHPMailer;
        use \PHPMailer\Exception;

        require 'PHPMailer/src/Exception.php';
        require 'PHPMailer/src/PHPMailer.php';
        require 'PHPMailer/src/SMTP.php';

		$mail = new phpmailer;
		$mail ->IsSMTP();
		$mail ->Host = 'smtp.gmail.com';
		$mail ->Port = '465';
		$mail ->SMPTAuth = true;
		$mail ->Username ="Write your username";
		$mail ->Password = 'SMPT password';
		$mail ->SMPTSecure = 'ssl';
        $mail ->From = $_POST["email"];
        $mail ->FromName =  $_POST["name"];
        $mail ->AddAddress('puritykamene21@gmail.com','Purity');
        $mail ->AddCC($_POST["email"],$_POST["name"]);
        $mail ->WordWrap = 50;
        $mail ->IsHTML(true);
        $mail ->Subject = $_POST["subject"];
        $mail ->Body = $_POST["message"];
        if ($mail ->Send()) 
        {
        	$error.='<p> <label class = "text-success">Thank you for Contacting us</label></p>';	       
        }
	else
	{
		$error.='<p> <label class = "text-danger">Theres an error</label></p>';	        
	}
	$name = '';
	$email = '';
	$subject = '';
	$message = '';
  }
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
    <meta name="description" content="(^_^) hahaa">
    <meta name="author" content="Stanley Onchari">
	<title>email</title>
	    <link href="./css/bootstrap.css" rel="stylesheet">
    <script src="js/js/jquery"></script>
<script src="./js/js/bootstrap.min.js"></script>

    <!-- Custom CSS -->
    <link href="./css/simple-sidebar.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="style.css">
	
	<!-- TABLE STYLES-->
    <link href="./css/dataTables/dataTables.bootstrap.css" rel="stylesheet" />

	<!-- just fonts  -->
	<link href="https://fonts.googleapis.com/css?family=Bree+Serif" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Fira+Sans" rel="stylesheet">

	<!-- SWEET ALERT -->
	<link href="./css/sweetalert.css" rel="stylesheet" type="text/css"/>
		
	<!--- JQUERY UI  -->
		<link href="./css/jquery-ui.css" rel='stylesheet' type='text/css'/>
		<link href="./css/jquery-ui.theme.css" rel='stylesheet' type='text/css'/>

    <!-- jQuery -->
    <script src="./js/jquery.js"></script>
	<script src="./js/sweetalert.js"></script>
</head>
<body>
<br/>
<div class="container">
	<div class="row">
		<div class="col-md- 8" style="margin: 0 auto; float : none;">
			<h3> send email using php mailer</h3><br/>
			<?php  echo $error; ?>
			 <form method="post">
             <div class="form-group">
             	<label>Enter name</label>
             	<input type="text" name="email" placeholder="Enter name" class="form-control" value="<?php  echo $name; ?>
">
             </div>
            <div class="form-group">
             	<label>Enter email</label>
             	<input type="text" name="email" placeholder="Enter email" class="form-control" value="<?php  echo $email; ?>">
            </div>
            <div class="form-group">
               <label>Enter Subject</label>
               <input type="text" name="subject" placeholder="Enter subject" class="form-control" value="<?php  echo $subject; ?>">
            </div>
            <div class="form-group">
               <label>Enter Message</label>
               <textarea name="message" class="form-control" placeholder="Enter message" value=" <?php  echo $message; ?>"></textarea>
            </div>
            <div class="form-group" align="center">
              
               <input type="submit" name="submit" value="Submit">
            </div>

			 </form>
 		</div>
	</div>
</div>

</body>
</html>