<?php 
include('../database_connection.php');
?>
<?php 
if (isset($_POST['export_excel'])) {
	$connect = mysqli_connect('localhost', 'root', '', 'stock_system');
	$sql = "SELECT request_details.status,request_details.department_name,request_details.product_name,user_details.user_id, user_details.first_name,user_details.last_name,request_details.request_id,request_details.request_priority,request_details.request_date,request_details.product_quantity FROM request_details
JOIN user_details ON 
 request_details.user_id = user_details.user_id
WHERE status ='Approved'  ORDER BY request_id ASC";
	$result = mysqli_query($connect,$sql);
    $output = '';
    
	if (mysqli_num_rows($result)>0) {
		$output .='
        <table class="table" bordered="1">
        <tr>                <th>Request ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Department Name</th>
                            
                            <th>Product Name</th>
                            <th>Product Quantity</th>
                            <th>Request Date</th>
                            <th>Priority Status</th>
                            <th>Status</th>
        </tr>
		'; 
		while ($row =mysqli_fetch_array($result)) {
					$output .='
                   <tr>
                   <td>' . $row['request_id']. '</td>
                    <td>' .$row['first_name']. '</td>
                    <td>' .$row['last_name']. '</td>
                    <td>' .$row['department_name']. '</td>
                    <td>' .$row['product_name']. '</td>
                    <td>' .$row['product_quantity'].'</td>
                    <td>' .$row['request_date']. '</td>
                    <td>' .$row['request_priority']. '</td>
                    <td>' .$row['status'].'</td>
                   </tr>
					';
		}
        $output .='</table>';
        header("Content-Type:application/xls");
        header("Content-Disposition:attachment; filename=download.xls");
        echo $output;
	}
}

 ?>