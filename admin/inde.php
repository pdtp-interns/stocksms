<?php 
include('../database_connection.php'); 
include('sidewrapper.php');
include('header.php');
?>
  <?php ERROR_REPORTING(E_ERROR || E_PARSE); ?>


<?php require_once 'manage_product_process.php'; ?>

	<?php 
if(isset($_SESSION['message'])) { ?>
<div class="alert alert-<?=$_SESSION['msg_type']?>">
	
<?php  
echo $_SESSION['message'];
unset($_SESSION['message']);
?>

</div>
<?php } ?>

<div class="container">
<?php 
$connect = mysqli_connect('localhost', 'root', '', 'stock_system');
$sql = "SELECT * FROM product_details";
$result = mysqli_query($connect,$sql);
$row =mysqli_fetch_assoc($result);

$product_id = $row["product_id"];

?>
	<div class="row justify-content-center">
	<table class="table">
		<thead>
			<tr>
				<th>Product Name</th>
				<th>Product Quantity</th>
				<th>Product Status</th>
				<th colspan="2">Action</th>
			</tr>
		</thead>
		<tbody>
 <?php
while ($row =mysqli_fetch_assoc($result)){ 


	?>
  <tr>
  	<td><?php echo $row['product_name']; ?></td>
  	<td><?php echo $row['product_quantity']; ?></td>
  	<td><?php echo $row['product_status']; ?></td>
  	<td>
  		<a href="inde.php?edit=<?php echo $row['product_id'] ?>" class="btn btn-info" >Edit</a>
  		<a href="manage_product_process.php?delete=<?php echo $row['product_id'];?>" class="btn btn-danger">Delete</a>

  	</td>
  </tr>
  <?php ERROR_REPORTING(E_ERROR || E_PARSE); ?>
<?php } ?>
</tbody>
	</table>
</div>
</div>

<?php
function pre_r($array) {
	echo '<pre>';
	print_r($array);
	echo '</pre>';
}
?>

	<div class="container" style="margin-top: 20px;">
  <div class="row justify-content-center">
	<form action="manage_product_process.php" method="POST">
        <input type="hidden" name="product_id" value="<?php echo $product_id; ?>">
		<div class="form-group">
		<label>Product Name</label>  
		<input type="text" name="product_name" class="form-control" value="<?php echo $product_name; ?>" placeholder="Type Product Name ">
		</div>
		<div class="form-group">
		<label>Product Quantity</label>
		<input type="text" name="product_quantity" class="form-control" value="<?php echo $product_quantity; ?>" placeholder="Product Quantity" >
		</div>
		<div class="form-group">
			<?php 
             if ($update == true):
			 ?>
			 <button type="submit" class="btn btn-info" name="update">Update</button>
			<?php else: ?>
		<button type="submit" class="btn btn-primary" name="submit">Add</button>
		<?php endif; ?>
			</div>

	</form>
   </div>
   </div>

</body>
</html>

<?php 
		if($row['product_quantity'] <= 10){
	    $query = "UPDATE product_details SET product_status='unavailable'";
    $result = mysqli_query($connect,$query);
}
 ?>
