<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="style.css">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
<title>  user login</title>
</head>
<body>

<div class="header">
	<h2>Login</h2>
</div>

<form method="post" action="#">
	<div class="input-group">
		<label>Username</label>
		<input type="text" name="username">
    </div>
	<div class="input-group">
		<label>Password</label>
		<input type="password" name="password">
    </div>
	<div class="input-group">
		<button type="submit" name="login" class="btn">login</button>
	</div>
<p>
	Not yet a  member?<a href="#">sign up</a>
</p>

</form>

</body>
</html>