
  <?php ERROR_REPORTING(E_ERROR || E_PARSE); ?>

<?php 
session_start();
include('database_connection.php');
include('header.php');
?>
<div class="page-container">  
   <div class="left-content">
     <div class="mother-grid-inner">
      <?php
      include('navbar.php');

      ?>
		<div class="container">
<?php 
$connect = mysqli_connect('localhost', 'root', '', 'stock_system');
$sql = "SELECT * FROM user_details";
$result = mysqli_query($connect,$sql);
//$row =mysqli_fetch_assoc($result);
$user_id = $row["user_id"];

?>
	<div class="row justify-content-center">
	<table class="table">
		<thead>
			<tr>
				<th>First Name</th>
				<th>Last Quantity</th>
				<th>Department Id</th>
				<th>User Role</th>
				<th>User Id</th>
				<th>User Email</th>
				<th>User Password</th>
				<th colspan="2">Action</th>
			</tr>
		</thead>
		<tbody>
 <?php
while ($row =mysqli_fetch_assoc($result)){ ?>
  <tr>
  	<td><?php echo $row['first_name']; ?></td>
  	<td><?php echo $row['last_name']; ?></td>
  	<td><?php echo $row['department_id']; ?></td>
  	<td><?php echo $row['user_role']; ?></td>
  	<td><?php echo $row['user_id']; ?></td>
  	<td><?php echo $row['user_email']; ?></td>
  	<td><?php echo $row['user_password']; ?></td>
  	<td>
  		<a href="edit_users.php?edit=<?php echo $row['user_id']; ?>" class="btn btn-info" >Edit</a>
  	    <a href="delete_users.php?delete=<?php echo $row['user_id'];?>" class="btn btn-danger">Delete</a>
  	</td>

  </tr>
  <?php ERROR_REPORTING(E_ERROR || E_PARSE); ?>
<?php } ?>
</tbody>
	</table>
</div>
</div>


</div>
</div>
</div>

<?php 
include('sidewrapper.php');
include('footer.php');
?>
