<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php 
include('database_connection.php');
session_start();

if(!isset($_SESSION["user_id"])){
  header('Location:index.php');
}
?>


<!DOCTYPE HTML>
<html>
<head>
<title>Automated Stock Management System</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Shoppy Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="files/css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="css/simple-sidebar.css" rel="stylesheet">
<!-- Custom Theme files -->
<link href="files/css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!--js-->
<script src="files/js/jquery-2.1.1.min.js"></script> 
<!--icons-css-->
<link href="files/css/font-awesome.css" rel="stylesheet"> 
<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Work+Sans:400,500,600' rel='stylesheet' type='text/css'>
<!--static chart-->
<script src="files/js/Chart.min.js"></script>
<!--//charts-->

<!-- geo chart -->
    <script src="//cdn.jsdelivr.net/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
    <script>window.modernizr || document.write('<script src="lib/modernizr/modernizr-custom.js"><\/script>')</script>
    <!--<script src="lib/html5shiv/html5shiv.js"></script>-->
     <!-- Chartinator  -->
    <script src="files/js/chartinator.js" ></script>
    <script type="files/text/javascript">
        jQuery(function ($) {

            var chart3 = $('#geoChart').chartinator({
                tableSel: '.geoChart',

                columns: [{role: 'tooltip', type: 'string'}],
         
                colIndexes: [2],
             
                rows: [
                    ['China - 2015'],
                    ['Colombia - 2015'],
                    ['France - 2015'],
                    ['Italy - 2015'],
                    ['Japan - 2015'],
                    ['Kazakhstan - 2015'],
                    ['Mexico - 2015'],
                    ['Poland - 2015'],
                    ['Russia - 2015'],
                    ['Spain - 2015'],
                    ['Tanzania - 2015'],
                    ['Turkey - 2015']],
              
                ignoreCol: [2],
              
                chartType: 'GeoChart',
              
                chartAspectRatio: 1.5,
             
                chartZoom: 1.75,
             
                chartOffset: [-12,0],
             
                chartOptions: {
                  
                    width: null,
                 
                    backgroundColor: '#fff',
                 
                    datalessRegionColor: '#F5F5F5',
               
                    region: 'world',
                  
                    resolution: 'countries',
                 
                    legend: 'none',

                    colorAxis: {
                       
                        colors: ['#679CCA', '#337AB7']
                    },
                    tooltip: {
                     
                        trigger: 'focus',

                        isHtml: true
                    }
                }

               
            });                       
        });
    </script>
<!--geo chart-->

<!--skycons-icons-->
<script src="files/js/skycons.js"></script>
<!--//skycons-icons-->



<script>
         function getproduct(str) {
              if (str.length == 0) { 
                 document.getElementById("txtHint").innerHTML = "";
                 return;
             } else {
                 var xmlhttp = new XMLHttpRequest();
                 xmlhttp.onreadystatechange = function() {
                     if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                         document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
                     }
                 };
                 xmlhttp.open("GET", "get_product.php?supplier_name=" + str, true);
                 xmlhttp.send();
             }
         }
     
     function getPrice(str) {
		
              if (str.length == 0) { 
                 document.getElementById("txtprice").innerHTML = "";
                 return;
             } else {
                 var xmlhttp = new XMLHttpRequest();
                 xmlhttp.onreadystatechange = function() {
                     if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                         document.getElementById("txtprice").innerHTML = xmlhttp.responseText;
                     }
                 };
                 xmlhttp.open("GET", "get_charges.php?product_supplied_name=" + str, true);
                 xmlhttp.send();
             }
         }
		 function calTotal(str){
			
		var product_price = document.getElementById("product_price").value;
		
		
		total = product_price * str;
		document.getElementById("total").value = total;
		
		}
     </script>

</head>
       
<body>  
<div class="page-container">  
   <div class="left-content">
     <div class="mother-grid-inner">

<?php
include('navbar.php');
?>
<div class="container">
  <div class="row">
    <div class="col-md-10 mx-auto">
      <div class="card" style="box-shadow: 0 0 25px 0 lightgrey; margin-left: 1px;">
        <div class="card-header" style=" ">
          <h4>Purchase Order Form</h4>
        </div>
        <div class="card-body" >
          <form name="add_row" id="add_row" action="purchase_order_process.php" method="post" enctype="multipart/form-data">
            <div class="form-group row">
              <label class="col-sm-3" align="right">Order Date</label>
              <div class="col-sm-6">
                <input type="text" class="form-control form-control-sm" readonly  name="" value="<?php echo date("Y-d-m"); ?>">
              </div>
            </div>
               <div class="form-group row">
              <label class="col-sm-3" align="right">Supplier</label>
              <div class="col-sm-6">
                <select class="form-control form-control-sm"  name="supplier_name" onmouseup="getproduct(this.value)">
                  <option name="department_name1" value="" selected=" ">Select Supplier</option>
            <?php 
                                      $connect = mysqli_connect('localhost','root','','stock_system');
                                      $query = "select * FROM suppliers";
                                      $result = mysqli_query($connect,$query);
                                      while ($output = mysqli_fetch_assoc($result)) {
                                        $supplier_id2 = $output["supplier_id"]; 
                                        $supplier_name = $output["supplier_name"];                                     ?>
                                     <option value="<?php  echo $output["supplier_name"]; ?>"><?php echo $output["supplier_name"]; ?></option>
                                     <?php } ?>
                </select>
              </div><br><br>
			  
			  
			 
			  
              <div class="form-group row">
              <label class="col-sm-8" align="right">Purchase Order No.</label>
              <div class="col-sm-3">
                <input type="text" class="form-control form-control-sm" readonly  name="po_number" value="#42506">
              </div>
            </div>
            </div>
           </div><br>
            <div class="card" style="box-shadow: 0 0 15px 0 lightgrey; ">
              <div class="card-body">
                <h3>Purchase Order List</h3>
                <table  class="table table-striped table-bordered table-hover" id="dynamic_field" align="" style=" ">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th style="text-align: ;">Product Description</th>
                      <th style="text-align: ;">Quantity</th>
                      <th style="text-align: ;">Unit Price</th>
                      <th style="text-align: ;">Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><b id="number">1</b></td>
                      <td>

                        <!--<div class="form-group">
                          <select class="form-control" name="product_supplied_name">
                            <option>Select prouct</option>
                          </select>  
                      </div>
					  <select name="product_supplied_name" onchange="getPrice(this.value)">
						<option value="Books">Books</option>
						<option value="pens">pens</option>
						<option value="milk">milk</option>
					</select> -->
						  <div id="txtHint">	
					
						</div>
						
                      </td>
                      <td>
                        <div class="form-group">
                        <input type="text" name="product_quantity" class="form-control-sm" onkeyup="calTotal(this.value)">
						
                      </div>
                      </td>
					  
					  
                      <!--<td> 

                        <div class="form-group">
                            <div class="form-group">
								<?php
								//$supplier_name = 'Chandaria';
//$connect = mysqli_connect('localhost','root','','stock_system');
//$sql = "SELECT * FROM suppliers WHERE supplier_name ='$supplier_name'";
//$result = $connect->query($sql);
//$row = $result->fetch_assoc();
//$supplier_id = $row["supplier_id"];

//$connect = mysqli_connect('localhost','root','','stock_system');
			//$sql = "SELECT * FROM supplier_products WHERE supplier_id = '$supplier_id'";
			//$result = $connect->query($sql);
			//$row = $result->fetch_assoc();
				//$product_price = $row["product_price"];
				//$product_supplied_name = $row["product_supplied_name"];
								
									//$connect = mysqli_connect('localhost','root','','stock_system');
									//$sql = "SELECT * FROM supplier_products WHERE product_supplied_name = '$product_supplied_name'";
									//$result = $connect->query($sql);
									//while($row = $result->fetch_assoc()){
										//$product_price = $row["product_price"];
										
										?>
										<input type="text" name="product_price" value="<?php //echo $row["product_price"]; ?>"> 
										
										
										<?php
									//}
								?>
						</div>
							
                        </div> 
						
                      </td>-->
                      <td>
                        <div class="form-group">
                          <div id="txtprice">
					  
					  </div>
                        </div>
                      </td>
                      <td>
                           <div class="form-group">
                            <input type="text" class="form-control form-control-sm"  name="total" id="total"  value=" ">
                        </div>
                       </td>
                    </tr>
                  </tbody>
                </table><!--table ends-->
                <button id="submit" name="submit" style="width: 120px;" class="btn btn-success">Submit</button>
              </div> <!-- card body ends -->
            </div><!-- order list card ends -->
            <center style= "padding: 10px;">
                  <!-- <button id="add" name="add" style="width: 90px;" class="btn btn-success">Add</button>
 -->                </center>
             <!-- <button type="submit" id="submit" class="btn btn-primary" name="submit">Submit</button>
 -->
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!--slider menu-->
           <?php
include('sidewrapper.php');
include('footer.php');
?>