<?php ERROR_REPORTING(E_ERROR || E_PARSE); ?>

<?php 
	include('database_connection.php');
if(isset($_POST['submit'])) {

	$first_name = mysqli_real_escape_string($connect,$_POST['first_name']);
	$last_name = mysqli_real_escape_string($connect,$_POST['last_name']);
    $department_name = mysqli_real_escape_string($connect,$_POST['department_name']);
    $department_id = mysqli_real_escape_string($connect,$_POST['department_id']);
    
    //echo $department_name."kkkkkk4";
    $user_role = mysqli_real_escape_string($connect,$_POST['user_role']);
    $user_email = mysqli_real_escape_string($connect,$_POST['user_email']);
    $user_password = mysqli_real_escape_string($connect,$_POST['user_password']);

    $connect = mysqli_connect('localhost','root','','stock_system');
      $query2 = "select * FROM department WHERE department_name = '$department_name'";
      $result = mysqli_query($connect,$query2);
      $output = mysqli_fetch_assoc($result);
        $department_id = $output["department_id"]; 
        //echo $department_id."gggg";

  //check for empty fields
    if (empty($first_name) || empty($last_name) || empty($department_id) || empty($user_role)||  empty($user_email) || empty($user_password) ) {
    	header("location : register.php?register=empty");
	    exit();
    }
    else{
//check if input characters are valid
    	if (!preg_match("/^[a-zA-Z]*$/",$first_name) || !preg_match("/^[a-zA-Z]*$/",$last_name)) {
    		header("location : register.php?register=invalid");
	        exit();
    	}
    	else{
    		//check if email is valid
    		if (!filter_var( $user_email, FILTER_VALIDATE_EMAIL)) {
    			header("location : register.php?register=email");
	            exit();
    		}
    		else{
    			$sql = "SELECT * FROM user_details WHERE user_id='$user_id'";
    			$result = mysqli_query($connect, $sql);
    			$resultCheck = mysqli_num_rows($result);

    			if ($resultCheck > 0) {
    				header("location : register.php?register=usertaken");
	                exit();
    			}
    			else{
    				//hashing password
      				//$hashedPassword = password_hash($user_password, PASSWORD_DEFAULT);
    				//insert user to db
                // $sql ="SELECT department_id FROM department";
                // $result = mysqli_query($connect,$sql);
                // $row =mysqli_fetch_assoc($result);
                //  $department_id = $row["department_id"];

    				$sql = "INSERT INTO user_details(first_name,last_name,department_id,user_role,user_email,user_password) VALUES('$first_name','$last_name','$department_id','$user_role','$user_email','$user_password')";
    				mysqli_query($connect, $sql);
    				header("location: register.php?register=success");
	                exit();
    			}
    		}
    	}
    }    
}
else{
	header("location : register.php");
	exit();
}
?>