<!--slider menu-->
    <div class="sidebar-menu" style="height:1200px;">
        <div class="logo"> <a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> <a href="#"> <span id="logo" ></span> 
            <!--<img id="logo" src="" alt="Logo"/>--> 
        </a> </div>     
        <div class="menu">
          <ul id="menu" >
            <li id="menu-home" ><a href="dashboard.php"><i class="fa fa-tachometer"></i><span>Dashboard</span></a></li>
            <li><a href="dashboard.php"><i class="fa fa-home"></i><span>Home</span></a></li>
            <li><a href="#"><i class="fa fa-cogs"></i><span>Issued Products</span><span class="fa fa-angle-right" style="float: right"></span></a>
              <ul>
                <li><a href="fully.php">Fully</a></li>
                <li><a href="partially.php">Partially</a></li> 
                <li><a href="stock_history.php">Stock History</a></li>                            
              </ul>
            </li>
            <li id="menu-academico" ><a href="#"><i class="fa fa-file-text"></i><span>Store Products</span><span class="fa fa-angle-right" style="float: right"></span></a>
              <ul id="menu-academico-sub" >
                 <li id="menu-academico-boletim" ><a href="add_product.php">Add Products</a></li>
                <li id="menu-academico-avaliacoes" ><a href="manage_product.php">Manage Products</a></li>
                <li id="menu-academico-avaliacoes1" ><a href="purchase_order.php">Purchase Order</a></li>                              
              </ul>
            </li>

            <li id="menu-comunicacao" ><a href="#"><i class="fa fa-user"></i><span>System Users</span><span class="fa fa-angle-right" style="float: right"></span></a>
              <ul id="menu-comunicacao-sub" >
                <li id="menu-arquivos" ><a href="register.php">Add User</a></li>
                <li id="menu-arquivos" ><a href="manage_users.php">Manage Users</a></li>
              </ul>
            </li>
             <li id="menu-academico" ><a href="#"><i class="fa fa-file-text"></i><span>Departments</span><span class="fa fa-angle-right" style="float: right"></span></a>
              <ul id="menu-academico-sub" >
                 <li id="menu-academico-boletim" ><a href="add_department.php">Add Department</a></li>
                <li id="menu-academico-avaliacoes" ><a href="manage_department.php">Manage Department</a></li>               
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-envelope"></i><span>Mailbox</span><span class="fa fa-angle-right" style="float: right"></span></a>
               <ul id="menu-academico-sub" >
                  <li id="menu-academico-avaliacoes" ><a href="inbox.html">Inbox</a></li>
                  <li id="menu-academico-boletim" ><a href="inbox-details.html">Compose email</a></li>
                 </ul>
            </li>
            <li><a href="analysis.php"><i class="fa fa-bar-chart"></i><span>Reporting</span></a></li>

          </ul>
        </div>
   </div>
