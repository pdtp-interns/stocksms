<?php
include('header.php')
?>

<body>  
<div class="page-container">  
   <div class="left-content">
     <div class="mother-grid-inner">
<?php
include('navbar.php')
?>
    <!-- /script-for sticky-nav -->

<div class="panel panel-default" style="margin-top: 15px;">
              <!-- Default panel contents -->
              <div class="panel-heading bg-info">Actioned Requests</div>

              <div class="container-fluid">
                <?php
$connect = mysqli_connect('localhost', 'root', '', 'stock_system');
$sql = "SELECT request_details.status,request_details.department_name,request_details.product_name,user_details.user_id, user_details.first_name,user_details.last_name,request_details.request_id,request_details.request_priority,request_details.request_date,request_details.product_quantity FROM request_details
JOIN user_details ON 
 request_details.user_id = user_details.user_id
WHERE status ='Approved' ";
$result = mysqli_query($connect,$sql);
?>
<div class="row justify-content-center">
<div class="table-responsive">
             <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
 
                       <tr>
                        <th>Action</th>
                            <th>Request ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Department Name</th>
                            <th>Product Name</th>
                            <th>Product Quantity</th>
                            <th>Request Date</th>
                            <th>Priority Status</th>
                            <th>Status</th>
                            
                        </tr>
                    </thead>
                      <tbody>
                          <?php
while ($row =mysqli_fetch_assoc($result)){ ?>
<form action="Approved_records.php" method="POST">
  <tr>
     <td><input type="checkbox" name="request_id" value="<?php echo $row['request_id']?>"</td>
    <td><?php echo $row['request_id']; ?></td>
    <td><?php echo $row['first_name']; ?></td>
    <td><?php echo $row['last_name']; ?></td>
    <td><?php echo $row['department_name']; ?></td>
    <td><?php echo $row['product_name']; ?></td>
    <td><?php echo $row['product_quantity']; ?></td>
    <td><?php echo $row['request_date']; ?></td>
    <td><?php echo $row['request_priority']; ?></td>
    <td><?php echo $row['status']; ?></td>
  </tr>
  <?php ERROR_REPORTING(E_ERROR || E_PARSE); ?>
<?php



 }

 ?>
                      </tbody>
             </table>

    </form>
         <?php // include_once('excel.php'); ?>
</div>
</div>
</div>
</div>
<?php
                        
?>
  <!--slider menu-->
<?php
include('sidewrapper.php');
?>
<!--copy rights start here-->
<?php
include('footer.php');
?>