    <div class="sidebar-menu" style="height:640px;">
		  	<div class="logo"> <a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> <a href="#"> <span id="logo" ></span> 
			      <!--<img id="logo" src="" alt="Logo"/>--> 
			  </a> </div>		  
		    <div class="menu">
		      <ul id="menu" >
		        <li id="menu-home" ><a href="manage_approvals.php"><i class="fa fa-tachometer"></i><span>Dashboard</span></a></li>
            <li><a href="manage_approvals.php"><i class="fa fa-home"></i><span>Home</span></a></li>

            <li><a href="manage_po.php"><i class="fa fa-product-hunt"></i><span>Purchase Orders</span></a></li>

              <li id="menu-academico" ><a href="#"><i class="fa fa-check"></i><span>Approval History</span><span class="fa fa-angle-right" style="float: right"></span></a>
              <ul id="menu-academico-sub" >
                 <li id="menu-academico-boletim" ><a href="approved.php">Approved</a></li>
                <li id="menu-academico-avaliacoes" ><a href="rejected.php">Rejected</a></li> 
                <li id="menu-academico-avaliacoes" ><a href="approved_records.php">All</a></li>              
              </ul>
            </li>

	
 	    <li><a href="charts.html"><i class="fa fa-bar-chart"></i><span>Reporting</span></a></li>

		      </ul>
		    </div>
	 </div>
	<div class="clearfix"> </div>