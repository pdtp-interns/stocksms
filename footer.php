<div class="clearfix"> </div>
<!--copy rights start here-->
<div class="copyrights" style="margin-top: 300px;px;">
   <p>© 2020 KNH Stock Management System. All Rights Reserved | Design by PDTP Interns</p>
</div>  
<!--COPY rights end here-->

</div>
<!--slide bar menu end here-->
<script>
var toggle = true;
            
$(".sidebar-icon").click(function() {                
  if (toggle)
  {
    $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
    $("#menu span").css({"position":"absolute"});
  }
  else
  {
    $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
    setTimeout(function() {
      $("#menu span").css({"position":"relative"});
    }, 400);
  }               
                toggle = !toggle;
            });
</script>
<!--scrolling js-->
    <script src="files/js/jquery.nicescroll.js"></script>
    <script src="files/js/scripts.js"></script>
    <!--//scrolling js-->
<script src="files/js/bootstrap.js"> </script>
<!-- mother grid end here-->

</body>
</html>                     