<!DOCTYPE html>
<html lang="en">
  <head>
    <title>KNH &mdash;Automated Stock System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,700,900|Display+Playfair:200,300,400,700"> 
    <link rel="stylesheet" href="index/fonts/icomoon/style.css">

    <link rel="stylesheet" href="index/css/bootstrap.min.css">
    <link rel="stylesheet" href="index/css/magnific-popup.css">
    <link rel="stylesheet" href="index/css/jquery-ui.css">
    <link rel="stylesheet" href="index/css/owl.carousel.min.css">
    <link rel="stylesheet" href="index/css/owl.theme.default.min.css">

    <link rel="stylesheet" href="index/css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="index/fonts/flaticon/font/flaticon.css">



    <link rel="stylesheet" href="index/css/aos.css">

    <link rel="stylesheet" href="index/css/style.css">
    
  </head>
  <body>
  
  <div class="site-wrap">

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
    
    <header class="site-navbar py-3" role="banner">

      <div class="container">
        <div class="row align-items-center">
          
 <div class="col-12 col-md-10 d-none d-xl-block">
            <nav class="navbar navbar-expand-sm bg-info navbar-light" style=" ">
    <div class="container-fluid">
      <div class="navbar-header">
        <h5>KNH STOCK MANAGEMENT SYSTEM</h5>
               <!--  <img src=" " alt="ASMS LOGO" style="width:200px;height:60px; float: left;"> -->
      </div>
      <ul class="nav navbar-nav navbar-right">
        <li class="nav-item">
            <a class="nav-link" href="#">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Contact</a>
          </li>
      
    </ul>
 
    </div>

      </nav>
          </div>


          <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>

          </div>

        </div>
      </div>
      
    </header>

  

    <div class="site-blocks-cover overlay" style="background-image: url(warehouse1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row align-items-center justify-content-center text-center">
        <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">    
        <div class="col-md-4">
          <div class="free-quote bg-dark h-100" style="width: 450px; margin-top:80px;margin-left: 70px; ">
            <h2 class="my-4 heading  text-center">Sign In</h2>
            <form method="post" action="login_process2.php">
              <div class="form-group mb-4">
               <input type="text" class="form-control btn-block" required id="user_email" name="user_email" placeholder="Enter Email">
              </div>
               <div class="form-group">
                    <input type="password" class="form-control btn-block" id="user_password" name="user_password" placeholder="Enter Password" required="">
              </div>
              <div class="form-group">
                <input type="submit" name="submit" class="btn btn-info text-white py-2 px-4 btn-block" value="Login">  
              </div>
              <p>
  Not yet a  member?<a href="register.php">sign up</a>
</p>
            </form>
          </div>   
        </div>    
        </div>
        </div>
      </div>
    </div>  



  <script src="/indexjs/jquery-3.3.1.min.js"></script>
  <script src="index/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="index/js/jquery-ui.js"></script>
  <script src="index/js/popper.min.js"></script>
  <script src="index/js/bootstrap.min.js"></script>
  <script src="index/js/owl.carousel.min.js"></script>
  <script src="index/js/jquery.stellar.min.js"></script>
  <script src="index/js/jquery.countdown.min.js"></script>
  <script src="index/js/jquery.magnific-popup.min.js"></script>
  <script src="index/js/bootstrap-datepicker.min.js"></script>
  <script src="index/js/aos.js"></script>

  <script src="index/js/main.js"></script>
    
  </body>
</html>