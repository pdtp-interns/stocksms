<?php session_start(); ?>

<?php 
include('database_connection.php');
 ?> 

<ul class="nav navbar-nav navbar-right">
     <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><?php echo $_SESSION['user_email']; ?><b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li>
          <a href="#"><i class="fa fa-fw fa-user"></i>profile</a>
        </li>
        <li class="divider"></li>
        <li>
          <a href="logout.php"><i class="fa fa-fw fa-power-off"></i>log out</a>
        </li>
      </ul> 
     </li>
  </ul>

 