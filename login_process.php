<?php 
include('database_connection.php');
//ERROR_REPORTING(E_ERROR || E_PARSE);

session_start();
if (isset($_POST['submit'])) {
	
	$user_email = mysqli_real_escape_string($connect,$_POST['user_email']);
	$user_password = mysqli_real_escape_string($connect,$_POST['user_password']);
	//check if inputs are empty
	if (empty($user_email) || empty($user_password)) {
		header("location : index.php?index=empty");
	    exit();

	}
	else{
		$sql = "SELECT * FROM user_details WHERE user_id='$user_email'";
		$result = mysqli_query($connect, $sql);
		$resultCheck = mysqli_num_rows($result);
		if ($resultCheck < 1) {
		    header("location : index.php?index=error");
	        exit();
           }
           else{
           	if ($row = mysqli_fetch_assoc($result)) {
           		$_SESSION['first_name'] = $row['first_name'];
           		$_SESSION['last_name'] = $row['last_name'];
           		$_SESSION['department_name'] = $row['department_name'];
           		$_SESSION['user_role'] = $row['user_role'];
           		$_SESSION['user_id'] = $row['user_id'];
           		$_SESSION['user_email'] = $row['user_email'];

           		header("location : index.php?index=success");
	            exit();
           	}
           }
		}
	}
else{
        header("location : index.php?index=error");
	    exit();
}

 ?>