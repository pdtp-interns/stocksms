<?php 
include('header.php');


?>
       
<body>  
<div class="page-container">  
<div class="left-content">
<div class="mother-grid-inner">

<?php
include('navbar.php');
?>

<div class="container">
  <div class="row">
    <div class="col-md-10 mx-auto">
      <div class="card" style="box-shadow: 0 0 25px 0 lightgrey; margin-left: 1px;">
        <div class="card-header">
          <h4>New Request</h4>
        </div>
        <div class="card-body">
          <form name="add_row" id="add_row" action="make_request_process.php" method="post" enctype="multipart/form-data" style=" margin-left: 5px; width: 850px;">
            <div class="form-group row">
              <label class="col-sm-3" >Request Date</label>
              <div class="col-sm-6">
                <input type="text" class="form-control form-control-sm" readonly  name="" value="<?php echo date("Y-d-m"); ?>">
              </div>
            </div>

            <div class="card" style="box-shadow: 0 0 15px 0 lightgrey; ">
              <div class="card-body">
                <h3>Make a order list</h3>
                <table  class="table table-striped table-bordered table-hover" id="dynamic_field" align="" style=" ">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th style="text-align: ;">Product name</th>
                      <th style="text-align: ;">Product quantity</th>
                      <th style="text-align: ;">Request Priority</th>
                      <!-- <th style="text-align: ;">Action</th> -->
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><b id="number">1</b></td>
                      <td>
                        <select name="product_name" id="product_name" class="form-control-sm" required style="width: 150px;">
                          <option name="product_name" value="" selected=" ">select product</option>
                                        <?php 
                                      $connect = mysqli_connect('localhost','root','','stock_system');
                                      $query = "SELECT * FROM product_details";
                                      $result = mysqli_query($connect,$query);
                                      while ($output = mysqli_fetch_assoc($result)) {
                                          $product_name = $output["product_name"];
                                     ?>
                                     <option value="<?php  echo $output["product_name"]; ?>"><?php echo $output["product_name"]; ?></option>
                                     <?php } ?>
                        </select>
                      </td>
                      <td>
                        <input type="text" name="product_quantity"  required class="form-control-sm"></td>
                      <td> 
                        <div class="form-group">
                          <label for="priority" name="">Priority:</label>
                            <label class="radio-inline" style="padding-left: 20px;"><input type="radio" value="low" name="request_priority">Low
                            </label>
                          <div class="radio">
                               <label style="padding-left: 83px;"><input type="radio" name="request_priority" value="medium" >Medium
                               </label>
                          </div>
                          <div class="radio-inline">
                               <label style="padding-left: 65px;"><input type="radio" name="request_priority" value="high">High</label>
                          </div>
                        </div>
                      </td>
                      <!-- <td> <button id="remove" style="width: 90px;" class="btn btn-danger btn_remove">Remove</button></td> -->
                    </tr>
                  </tbody>
                </table><!--table ends-->
                
              </div> <!-- card body ends -->
            </div><!-- order list card ends -->
          <center style= "padding: 10px;">
                  <!-- <button id="add" name="add" style="width: 90px;" class="btn btn-success">Add</button>
 -->                </center>
             <button type="submit" id="submit" class="btn btn-success" name="submit">Submit</button>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!--slider menu-->
    
    <?php
include('sidewrapper.php');
include('footer.php');
?>
