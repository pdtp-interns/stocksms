
<?php 
include('database_connection.php');
include('header.php');
?>
<div class="page-container" style="">  
   <div class="left-content">
<div class="mother-grid-inner" style="margin-left: 0px;">
      <?php
      include('navbar.php');
      ?>

<div class="header">
	<h2>Register</h2>
</div>

<form method="post" action="register_process.php">
	<div class="input-group">
		<label>First Name</label>
		<input type="text" name="first_name" value="" required="">
    </div>
    <div class="input-group">
		<label>Last Name</label>
		<input type="text" name="last_name" value="">
    </div>
     	<div class="input-group">
		<label>Department</label>
		   <select name="department_name" class="form-control"> 
          <option name="department_name1" value="" selected=" ">Finance</option>
            <?php 
                                      $connect = mysqli_connect('localhost','root','','stock_system');
                                      $query = "select * FROM department";
                                      $result = mysqli_query($connect,$query);
                                      while ($output = mysqli_fetch_assoc($result)) {
                                        $department_id = $output["department_id"]; 
                                        $department_name = $output["department_name"];                                     ?>
                                     <option name="department_name1" value="<?php  echo $output["department_name"]; ?>"><?php echo $output["department_name"]; ?></option>
                                     <?php } ?>
        </select>
    </div>
     <div class="input-group">
		<label>User Role</label>
		<select name="user_role" class="form-control"> 
         <option name="user_role" value="Staff" selected="">Staff</option>
         <option name="user_role" value="Approver" selected=" ">Approver</option>
         <option name="user_role" value="Admin">Admin</option>
     </select>

    </div>
	<div class="input-group">
		<label>Email</label>
		<input type="email" name="user_email" value="" required="">
    </div>
	<div class="input-group">
		<label>Password</label>
		<input type="password" name="user_password">
    </div>
	
  <div class="input-group">
    <button type="submit" name="submit" class="btn">Register</button>
  </div>

 
  
</form>

</div>
</div>
</div>

<?php 
include('footer.php');
?>