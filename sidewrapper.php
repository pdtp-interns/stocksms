    <div class="sidebar-menu" style="height:1200px;">
        <div class="logo"> <a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> <a href="#"> <span id="logo" ></span> 
            <!--<img id="logo" src="" alt="Logo"/>--> 
        </a> </div>     
        <div class="menu">
          <ul id="menu" >
            <li id="menu-home" ><a href="new_order.php"><i class="fa fa-tachometer"></i><span>Dashboard</span></a></li>
            <li><a href="new_order.php"><i class="fa fa-home"></i><span>Home</span></a></li>

            <li><a href="my_request.php"><i class="fa fa-history"></i><span>Request History</span></a>
             
            </li>
            <li id="menu-academico" ><a href="#"><i class="fa fa-bell"></i><span>Approver Notifications</span><span class="fa fa-angle-right" style="float: right"></span></a>
              <ul id="menu-academico-sub" >
                 <li id="menu-academico-boletim" ><a href="approved.php">Approved</a></li>
                <li id="menu-academico-avaliacoes" ><a href="rejected.php">Rejected</a></li> 
                <li id="menu-academico-avaliacoes" ><a href="pending.php">Pending</a></li>              
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-product-hunt"></i><span>Product History </span><span class="fa fa-angle-right" style="float: right"></span></a>
               <ul id="menu-academico-sub" >
                  <li id="menu-academico-avaliacoes" ><a href="fully.php">Fully issued</a></li>
                  <li id="menu-academico-boletim" ><a href="partially.php">Partially issued</a></li>
                 </ul>
            </li>
            <li><a href="analysis.php"><i class="fa fa-bar-chart"></i><span>Analytics</span></a></li>

          </ul>
        </div>
   </div>